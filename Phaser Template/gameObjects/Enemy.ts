/// <reference path = "../lib/phaser.d.ts"/>

module Enemy {
    const ENEMY_RADIUS = 10;

    export enum EnemyStates{
        IDLE, GESTURE, WALK, ATTACK, DEATH
    };

    export class Enemy{
        velocity: number;
        health: number;
        armor: number;
        damage: number;
        currentState: EnemyStates;
        isDead: boolean;


        constructor(velocity, health, armor, damage){
            this.velocity = velocity;
            this.health = health;
            this.armor = armor;
            this.damage = damage;
            this.currentState = EnemyStates.IDLE;
            this.isDead = false;
        }
    }
}
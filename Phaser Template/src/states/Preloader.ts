/// <reference path = "../../lib/phaser.d.ts"/>

module GameName {
    export class Preloader extends Phaser.State{

        preload() {
            this.load.tilemap("town", "src/assets/tilemaps/maps/town.json", null, Phaser.Tilemap.TILED_JSON);
            this.load.image("tiles", "src/assets/tilemaps/tiles/Castle2.png");
            // this.load.spritesheet("Goblins", "src/assests/Sprites/goblinMelee.png", 32, 32);
            this.load.spritesheet("Warrior", "src/assets/Sprites/warriorHero.png", 32, 32);
        }

        create() {
            this.game.state.start("Game");
        }
    }
}
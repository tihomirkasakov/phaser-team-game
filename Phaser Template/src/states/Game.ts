/// <reference path = "../../lib/phaser.d.ts"/>

module GameName{
    export class Game extends Phaser.State{
        // ADD CLASS PROPERTIES HERE
        game: Phaser.Game;
        player;
        cursor;
        map: Phaser.Tilemap;
        collisionBlocks: Phaser.TilemapLayer;
        decor: Phaser.TilemapLayer;
        decor2: Phaser.TilemapLayer;
        group: Phaser.Group;
        floor: Phaser.TilemapLayer;

        create(){
            // INITIALIZE GAME ELEMENTS HERE
            let floor;
            let layers;

            this.map = this.game.add.tilemap("town");
            this.map.addTilesetImage("Town", "tiles");
            this.floor = this.map.createLayer('floor');
            this.collisionBlocks = this.map.createLayer("collision");
            this.map.setCollisionBetween(0, 10000, true, this.collisionBlocks);
            this.decor = this.map.createLayer("decor");
            this.decor2 = this.map.createLayer("decor2");
            this.floor.scale.set(2);
            this.floor.resizeWorld();

            let position = this.getObjectPosition(this.map);
            console.log(position);
            this.player = this.game.add.sprite(position.x, position.y, "Warrior");
            this.game.physics.enable(this.player, Phaser.Physics.ARCADE);
            this.game.camera.follow(this.player);
            this.cursor = this.game.input.keyboard.createCursorKeys();

            this.group = this.game.add.group();
            this.group.add(this.player);
            this.group.add(this.decor);
            this.group.add(this.decor2);

        }

        update(){
            // THIS METHOD IS CALLED 60 TIMES A SECOND. PUT YOUR GAME LOGIC HERE
            this.game.physics.arcade.collide(this.player, this.collisionBlocks);
            this.player.body.velocity.y = 0;
            this.player.body.velocity.x = 0;

            if(this.cursor.up.isDown) {
                this.player.body.velocity.y -= 300;
            }
            else if(this.cursor.down.isDown){
                this.player.body.velocity.y += 300;
            }
            else if(this.cursor.left.isDown) {
                this.player.body.velocity.x -= 300;
            }
            else if(this.cursor.right.isDown){
                this.player.body.velocity.x += 300;
            }
            this.group.sort('z', Phaser.Group.SORT_ASCENDING);
        }

        render(){
            // USED FOR DEBUGGING, e.g

        }

        getObjectPosition(map) {
            let position;
            map.objects["entities"].forEach(element => {
                if(element.name === "hero"){
                    position = element;
                }
            })
            return position;
        }
    }
}